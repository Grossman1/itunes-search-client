import React, {Component} from 'react';
import './App.css';
import {SearchHome} from "./components/search/SearchHome";
import {browserHistory, Route, Router} from "react-router"
import {ItunesItem} from "./components/itunes-item/ItunesItem";

class App extends Component {
    render() {
        return (
            <div className={'container'}>
                <nav className={'navbar navbar-tlight'}>
                    <a className={'navbar-brand'} href="http://www.anyvision.co">AnyVision</a>
                    <a href={"/"} className={'navbar-brand'}>Itunes Search</a>
                    <h2 className={'navbar-brand'}>Omri Grossman</h2>
                </nav>
                <Router history={browserHistory}>
                    <Route path={"/"} component={SearchHome}/>
                    <Route path={"videoDetails"} component={ItunesItem}/>
                </Router>
            </div>
        );
    }
}

export default App;
