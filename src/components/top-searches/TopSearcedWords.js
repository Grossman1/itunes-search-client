import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './TopSearches.css'
import {ItunesItemDetails} from "../itunes-item/ItunesItem";
const axios = require('axios');

export class TopSearcedWords extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mostSearchedWords: []
        }
    }

    onFetchMostSearchedWords() {
        axios.get('http://localhost:3001/searchTerms/top10')
            .then(response => {
                const mostSearchedWordsArray = this.handleMostSearchedWordsResponse(response);
                this.setMostSearchedWords(mostSearchedWordsArray);
            })
    }

    handleMostSearchedWordsResponse(response) {
        const mostSearchedWordsArray = [];
        response.data['obj']
            .map(item => {
                mostSearchedWordsArray.push(item._id);
            });
        return mostSearchedWordsArray;
    }

    setMostSearchedWords(mostSearchedWordsArray) {
        this.setState({
            mostSearchedWords: mostSearchedWordsArray
        });
    }

    render() {
        return (
            <div className={'container'}>
                <div className={'row'}>
                    <button onClick={this.onFetchMostSearchedWords.bind(this)} className={'btn btn-success'}>
                        Click ere to see top 10 most searched words!
                    </button>
                </div>
                <div className={'row top-words-container'}>
                    {this.state.mostSearchedWords.map((resultItem, i) =>
                        <span className={'badge badge-info top-word-badge'} key={i}>{resultItem}</span>)}
                </div>
            </div>
        );
    }
}

TopSearcedWords.propTypes = {
    optionalArray: PropTypes.array
};