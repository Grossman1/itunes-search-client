import React, {Component} from 'react';
import {SearchForm} from "./SearchForm";
import {SearchResultList} from "./SearchResultList";
import PropTypes from 'prop-types';
import {TopSearcedWords} from "../top-searches/TopSearcedWords";

export class SearchHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchResults: []
        }
    }

    onSearchResult(fetchedSearchResults) {
        this.setState({
            searchResults: fetchedSearchResults
        });
    }

    render() {

        return (
            <div className={'row'}>
                <div className={'col-md-8'}>
                    <div className={'row'}>
                        <SearchForm onSearchResult={this.onSearchResult.bind(this)}/>
                    </div>
                    <div className={'row'}>
                        <SearchResultList searchResults={this.state.searchResults}/>
                    </div>
                </div>
                <div className={'col-md-4'}>
                    <TopSearcedWords/>
                </div>
            </div>
        );
    }
}

SearchResultList.propTypes = {
    searchResults: PropTypes.array,
};

