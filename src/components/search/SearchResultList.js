import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {SearchResultItem} from "./SearchResultItem";

export class SearchResultList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchResults: props.searchResults
        };
    }

    componentWillReceiveProps(nextProp) {
            this.setState({
                searchResults: nextProp.searchResults
            });
    }


    render() {
        return (
            <div className={'row'}>
                {this.state.searchResults.map((resultItem, i) =>
                    <SearchResultItem key={i} itemIndex={i} itunesResultItem={resultItem}/>)}
            </div>
        );
    }
}

SearchResultList.propTypes = {
    searchResults: PropTypes.array,
};
