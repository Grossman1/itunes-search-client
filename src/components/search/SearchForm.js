import React, {Component} from 'react';
import PropTypes from 'prop-types';

const axios = require('axios');

export class SearchForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchInput: ""
        }
    }

    onHandleSubmit() {
        this.onSearchItunes(this.state.searchInput)
    }

    onHandleChange(event) {
        this.setState({
            searchInput: event.target.value
        });
    }

    onSearchItunes(searchInput) {
        if (searchInput) {
            this.searchItunes(searchInput.toLowerCase());
        }
        else {
            alert('Please fill the input field!')
        }
    }

    /**
     * Calls the itunes search engine and searches the user's search input
     *
     * Returns a list of 50 itunes items
     * @param searchInput
     */
    searchItunes(searchInput) {
        axios.get('https://itunes.apple.com/search?term=' + searchInput)
            .then((response) => {
                this.props.onSearchResult(response.data.results);
                this.saveSearchWords(searchInput);
            });
    }

    /**
     * Sends the search input to the server in order to save it
     *
     * @param searchInput
     */
    saveSearchWords(searchInput) {
        axios.post('http://localhost:3001/searchTerms', {"searchTerms": searchInput})
            .then((response) => {
                if (response.data.obj) {
                    console.log('Data was saved!');
                }
            });
    }

    render() {
        return (
            <div className={'row search-form-container'}>
                <div className={'col-md-10'}>
                    <input placeholder={"What are you looking up for?"}
                           type="text"
                           className={'form-control'}
                           onChange={(event) => this.onHandleChange(event)}/>
                </div>
                <div className={'col-md-2'}>
                    <button className={'btn btn-primary'}
                            onClick={this.onHandleSubmit.bind(this)}>Search
                    </button>
                </div>
            </div>
        );
    }
}

SearchForm.propTypes = {
    searchInput: PropTypes.string,
};

