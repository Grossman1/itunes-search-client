import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './search.css';
import {Link} from "react-router";

export class SearchResultItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            itunesResultItem: props.itunesResultItem
        }
    }

    componentWillReceiveProps(nextProp) {
        this.setState({
            itunesResultItem: nextProp.itunesResultItem
        });
    }

    render() {
        return (
            <Link to={{
                pathname: "/videoDetails",
                state: {itunesResultItem: this.state.itunesResultItem}
            }} className={'card text-white bg-secondary mb-3 border-info video-card'}>
                <img className={'card-img-top video-card-image'}
                     src={this.state.itunesResultItem.artworkUrl100}/>
                <div className={'card-header'}>
                    {this.state.itunesResultItem.artistName}
                </div>
                <div className={'card-body'}>
                    <div className={'card-title'}>{this.state.itunesResultItem.primaryGenreName}</div>
                    <div className={'card-text'}>{this.state.itunesResultItem.trackName}</div>
                </div>
            </Link>
        );
    }
}

SearchResultItem.propTypes = {
    itunesResultItem: PropTypes.object,
    trackName: PropTypes.string,
    primaryGenreName: PropTypes.string,
    artistName: PropTypes.string,
    artworkUrl100: PropTypes.string,
};