import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class ItunesItemDetails extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'col-md-3'}>
                <h5>Collection Name:</h5> {this.props.collectionName}
                <h5>Collection
                    Price:</h5> {this.props.collectionPrice} {this.props.collectionPriceCurrency}
                <h5>Realesed Year:</h5> {this.props.releaseDate}
                <h5>Genere:</h5> {this.props.genere}
            </div>
        );
    }
}

ItunesItemDetails.propTypes = {
    collectionName: PropTypes.string,
    collectionPrice: PropTypes.string,
    collectionPriceCurrency: PropTypes.string,
    releaseDate: PropTypes.string,
    genere: PropTypes.string
};
