import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './ItunesDetail.css'
import {VideoPlayerComponent} from "./VideoPlayerComponent";
import {ItunesItemHeader} from "./ItunesItemHeader";
import {ItunesItemDetails} from "./ItunesItemDetails";

export class ItunesItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itunesItem: props.location.state.itunesResultItem
        };
        const releaseDateArray = this.state.itunesItem['releaseDate'].split('-');
        this.realeasedYear = releaseDateArray[0];
    }

    render() {
        return (
            <div className={'container'}>
                <ItunesItemHeader titleArtistName={this.state.itunesItem.artistName}
                                  titleTrackName={this.state.itunesItem.trackName}/>
                <div className="row video-details">
                    <VideoPlayerComponent videoUrl={this.state.itunesItem.previewUrl}/>
                    <ItunesItemDetails
                        collectionName={this.state.itunesItem.collectionName}
                        collectionPrice={this.state.itunesItem.collectionPrice}
                        collectionPriceCurrency={this.state.itunesItem.currency}
                        releaseDate={this.realeasedYear}
                        genere={this.state.itunesItem.primaryGenreName}/>
                </div>
            </div>
        );
    }
}

ItunesItem.propTypes = {
    itunesResultItem: PropTypes.object,
    previewUrl: PropTypes.string,
    currency: PropTypes.string
};