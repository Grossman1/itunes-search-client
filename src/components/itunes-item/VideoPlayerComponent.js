import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class VideoPlayerComponent extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className={'col-md-9'}>
                <video width="450" controls>
                    <source src={this.props.videoUrl} type="video/mp4"/>
                </video>
            </div>
        );
    }
}

VideoPlayerComponent.propTypes = {
    videoUrl: PropTypes.string
};

