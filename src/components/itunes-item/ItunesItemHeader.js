import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class ItunesItemHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'row'}>
                <h2 className={'video-title'}>{this.props.titleArtistName}
                    - {this.props.titleTrackName}</h2>
            </div>
        );
    }
}
ItunesItemHeader.propTypes = {
    titleArtistName: PropTypes.string,
    titleTrackName: PropTypes.string
};